//Caelan Whitter / 1841768

package lab02.eclipse;

public class BikeStore {

	public static void main(String[] args) {
		Bicycle [] bikes = new Bicycle[4];
		bikes[0]=new Bicycle("Raleigh",15,40);
		bikes[1]=new Bicycle("Focus",13,35);
		bikes[2]=new Bicycle("Felt",12,30);
		bikes[3]=new Bicycle("BMC",10,25);
		for(int i=0; i<bikes.length ; i++)
		{
			System.out.println(bikes[i]);
		}
	}

}
